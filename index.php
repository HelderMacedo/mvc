<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
session_start();
spl_autoload_register(function($class)
{
    if(file_exists('controllers/'.$class.'.php')){
        require_once 'controllers/'.$class.'.php';
    }
    else if(file_exists('core/'.$class.'.php')){
        require_once 'core/'.$class.'.php';
    }
    else if(file_exists('models/'.$class.'.php')){
        require_once 'models/'.$class.'.php';
    }
});

$core = new Core();
$core->run();

