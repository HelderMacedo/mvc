<?php

class Anuncios extends model
{
    
    public function getQuantidade()
    {
       $sql = "SELECT COUNT(*) as quantidade FROM epi";
       $sql = $this->db->query($sql);

       if($sql->rowCount() > 0){
            $sql = $sql->fetch();
            return $sql['quantidade'];
       }else{
           return 0;
       }

    }    
}